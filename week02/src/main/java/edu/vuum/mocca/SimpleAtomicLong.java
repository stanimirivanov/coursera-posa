package edu.vuum.mocca;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @class SimpleAtomicLong
 *
 * @brief This class implements a subset of the
 *        java.util.concurrent.atomic.SimpleAtomicLong class using a
 *        ReentrantReadWriteLock to illustrate how they work.
 */
class SimpleAtomicLong {
	/**
	 * The value that's manipulated atomically via the methods.
	 */
	private long mValue;

	/**
	 * The ReentrantReadWriteLock used to serialize access to mValue.
	 */

	// TODO -- you fill in here by replacing the null with an
	// initialization of ReentrantReadWriteLock.
	private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private final Lock readLock = readWriteLock.readLock();
	private final Lock writeLock = readWriteLock.writeLock();

	/**
	 * Creates a new SimpleAtomicLong with the given initial value.
	 */
	public SimpleAtomicLong(long initialValue) {
		// TODO -- you fill in here
		writeLock.lock();
		this.mValue = initialValue;
		writeLock.unlock();
	}

	/**
	 * @brief Gets the current value.
	 * 
	 * @returns The current value
	 */
	public long get() {
		// TODO -- you fill in here
		readLock.lock();
		try {
			return mValue;
		} finally {
			readLock.unlock();
		}
	}

	/**
	 * @brief Atomically decrements by one the current value
	 *
	 * @returns the updated value
	 */
	public long decrementAndGet() {
		// TODO -- you fill in here
		writeLock.lock();
		try {
			return --mValue;
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * @brief Atomically increments by one the current value
	 *
	 * @returns the previous value
	 */
	public long getAndIncrement() {
		// TODO -- you fill in here
		writeLock.lock();
		try {
			return mValue++;
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * @brief Atomically decrements by one the current value
	 *
	 * @returns the previous value
	 */
	public long getAndDecrement() {
		// TODO -- you fill in here
		writeLock.lock();
		try {
			return mValue--;
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * @brief Atomically increments by one the current value
	 *
	 * @returns the updated value
	 */
	public long incrementAndGet() {
		// TODO -- you fill in here
		writeLock.lock();
		try {
			return ++mValue;
		} finally {
			writeLock.unlock();
		}
	}
}